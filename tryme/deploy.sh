#!/bin/bash

sls deploy

sleep 2 

sub_sns="arn:aws:sns:us-east-1:909603365907:test"
temp=$(sls info | grep POST)
endpoint=$(echo $temp| sed 's/.* //')

sleep 2

aws sns subscribe --topic-arn "$sub_sns" --protocol https --notification-endpoint "$endpoint"

